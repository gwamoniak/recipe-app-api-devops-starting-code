variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "selukasz@gmail.com"
}

variable "db_username" {
  description = "Username for RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instaces"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}
